package com.messenger.messenger

import com.messenger.messenger.model.messagemodel.Message
import com.messenger.messenger.model.User
import com.messenger.messenger.model.messagemodel.FileMessage

interface UIEvent {
    /**
     * A group of functions that APIs call or Databases call
     * this functions are used by "business logic" classes to submit changes to UI.
     *
     */
    fun newMessages(message: Message)

    fun messageSeen(message: Message)

    fun lastSeenChanged(user:User, lastSeen:String)

    fun userIsOnline(isOnline: Boolean)

    fun fileStartedDownloading(fileMessage:FileMessage)
    fun fileCompletedDownloading(fileMessage:FileMessage)





}