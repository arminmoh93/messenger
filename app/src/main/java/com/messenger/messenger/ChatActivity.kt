package com.messenger.messenger

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.messenger.messenger.model.messagemodel.Message
import com.messenger.messenger.model.messagemodel.TextMessage
import com.messenger.messenger.model.User
import com.messenger.messenger.model.messagemodel.FileMessage

class ChatActivity : AppCompatActivity(), UIEvent {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }





    /**This functions  are used by "business logic" classes such as ViewModels that use APIs and DBs
     * you should fill these functions to submit different scenarios that happen on UI
     *
     */

    override fun newMessages(message: Message) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun messageSeen(message: Message) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun lastSeenChanged(user: User, lastSeen: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun userIsOnline(isOnline: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun fileCompletedDownloading(fileMessage: FileMessage) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun fileStartedDownloading(fileMessage: FileMessage) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }



    /**You should not fill these functions !! We will do that to connecting to LOGIC classes
     * You JUST call these functions in appropriate scenarios
     * For example : User click on send button and and we need you to call our LOGIC classes to send message
     *on APIs SO you should call sendMessage() method on Click Event :
     * example: sendButton.setOnClickListener{ sendMessage(  EditText.text.toString  )   }
     *
     *
     */
    fun sendMessage(message: TextMessage) {}


    //this function called when user swipe up the chat page
    // AND see OLDER MESSAGES not cached yet

    fun cachMoreMessage(lastCachedMessage: Message){}

}
