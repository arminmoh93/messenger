package com.messenger.messenger.model.messagemodel

import com.messenger.messenger.model.messagemodel.Message
import java.text.SimpleDateFormat
import java.util.*

class TextMessage(sender: String , val text:String ) : Message(
    UUID.randomUUID().toString(), "TEXT",
    SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(Date())
    , sender
)