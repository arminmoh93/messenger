package com.messenger.messenger.model.messagemodel

import java.text.SimpleDateFormat
import java.util.*

class FileMessage(sender: String) : Message(
    UUID.randomUUID().toString(), "FILE",
    SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(Date())
    , sender
)