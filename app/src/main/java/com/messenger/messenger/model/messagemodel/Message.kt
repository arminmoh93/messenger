package com.messenger.messenger.model.messagemodel

open  class Message(val id:String ,
                   val type:String  ,
                   val dateTime:String ,
                   val senderName:String)